package com.hero.wireless.web.entity.send;

import com.hero.wireless.web.entity.base.BaseEntity;
import java.util.Date;

public class InputAwait extends BaseEntity {
    private Long id;

    private String UUID;

    private String msg_Batch_No;

    private String enterprise_Msg_Id;

    private String enterprise_No;

    private String agent_No;

    private Integer business_User_Id;

    private Integer enterprise_User_Id;

    private String product_No;

    private Boolean is_LMS;

    private String data_Status_Code;

    private String message_Type_Code;

    private String content;

    private String charset;

    private String phone_Nos;

    private Integer phone_Nos_Count;

    private String protocol_Type_Code;

    private String country_Code;

    private String source_IP;

    private Integer priority_Number;

    private String audit_Status_Code;

    private Integer audit_Admin_User_Id;

    private Date audit_Date;

    private String sub_Code;

    private Date send_Time;

    private String gate_Ip;

    private String assist_Audit_Key;

    private String description;

    private String remark;

    private Date create_Date;

    private Date input_Date;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUUID() {
        return UUID;
    }

    public void setUUID(String UUID) {
        this.UUID = UUID;
    }

    public String getMsg_Batch_No() {
        return msg_Batch_No;
    }

    public void setMsg_Batch_No(String msg_Batch_No) {
        this.msg_Batch_No = msg_Batch_No;
    }

    public String getEnterprise_Msg_Id() {
        return enterprise_Msg_Id;
    }

    public void setEnterprise_Msg_Id(String enterprise_Msg_Id) {
        this.enterprise_Msg_Id = enterprise_Msg_Id;
    }

    public String getEnterprise_No() {
        return enterprise_No;
    }

    public void setEnterprise_No(String enterprise_No) {
        this.enterprise_No = enterprise_No;
    }

    public String getAgent_No() {
        return agent_No;
    }

    public void setAgent_No(String agent_No) {
        this.agent_No = agent_No;
    }

    public Integer getBusiness_User_Id() {
        return business_User_Id;
    }

    public void setBusiness_User_Id(Integer business_User_Id) {
        this.business_User_Id = business_User_Id;
    }

    public Integer getEnterprise_User_Id() {
        return enterprise_User_Id;
    }

    public void setEnterprise_User_Id(Integer enterprise_User_Id) {
        this.enterprise_User_Id = enterprise_User_Id;
    }

    public String getProduct_No() {
        return product_No;
    }

    public void setProduct_No(String product_No) {
        this.product_No = product_No;
    }

    public Boolean getIs_LMS() {
        return is_LMS;
    }

    public void setIs_LMS(Boolean is_LMS) {
        this.is_LMS = is_LMS;
    }

    public String getData_Status_Code() {
        return data_Status_Code;
    }

    public void setData_Status_Code(String data_Status_Code) {
        this.data_Status_Code = data_Status_Code;
    }

    public String getMessage_Type_Code() {
        return message_Type_Code;
    }

    public void setMessage_Type_Code(String message_Type_Code) {
        this.message_Type_Code = message_Type_Code;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCharset() {
        return charset;
    }

    public void setCharset(String charset) {
        this.charset = charset;
    }

    public String getPhone_Nos() {
        return phone_Nos;
    }

    public void setPhone_Nos(String phone_Nos) {
        this.phone_Nos = phone_Nos;
    }

    public Integer getPhone_Nos_Count() {
        return phone_Nos_Count;
    }

    public void setPhone_Nos_Count(Integer phone_Nos_Count) {
        this.phone_Nos_Count = phone_Nos_Count;
    }

    public String getProtocol_Type_Code() {
        return protocol_Type_Code;
    }

    public void setProtocol_Type_Code(String protocol_Type_Code) {
        this.protocol_Type_Code = protocol_Type_Code;
    }

    public String getCountry_Code() {
        return country_Code;
    }

    public void setCountry_Code(String country_Code) {
        this.country_Code = country_Code;
    }

    public String getSource_IP() {
        return source_IP;
    }

    public void setSource_IP(String source_IP) {
        this.source_IP = source_IP;
    }

    public Integer getPriority_Number() {
        return priority_Number;
    }

    public void setPriority_Number(Integer priority_Number) {
        this.priority_Number = priority_Number;
    }

    public String getAudit_Status_Code() {
        return audit_Status_Code;
    }

    public void setAudit_Status_Code(String audit_Status_Code) {
        this.audit_Status_Code = audit_Status_Code;
    }

    public Integer getAudit_Admin_User_Id() {
        return audit_Admin_User_Id;
    }

    public void setAudit_Admin_User_Id(Integer audit_Admin_User_Id) {
        this.audit_Admin_User_Id = audit_Admin_User_Id;
    }

    public Date getAudit_Date() {
        return audit_Date;
    }

    public void setAudit_Date(Date audit_Date) {
        this.audit_Date = audit_Date;
    }

    public String getSub_Code() {
        return sub_Code;
    }

    public void setSub_Code(String sub_Code) {
        this.sub_Code = sub_Code;
    }

    public Date getSend_Time() {
        return send_Time;
    }

    public void setSend_Time(Date send_Time) {
        this.send_Time = send_Time;
    }

    public String getGate_Ip() {
        return gate_Ip;
    }

    public void setGate_Ip(String gate_Ip) {
        this.gate_Ip = gate_Ip;
    }

    public String getAssist_Audit_Key() {
        return assist_Audit_Key;
    }

    public void setAssist_Audit_Key(String assist_Audit_Key) {
        this.assist_Audit_Key = assist_Audit_Key;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getCreate_Date() {
        return create_Date;
    }

    public void setCreate_Date(Date create_Date) {
        this.create_Date = create_Date;
    }

    public Date getInput_Date() {
        return input_Date;
    }

    public void setInput_Date(Date input_Date) {
        this.input_Date = input_Date;
    }
}